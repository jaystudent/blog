class Comment < ActiveRecord::Base
  belongs_to :post
  validates :post, presence: true
  validates_presence_of :post_id
  validates_presence_of :body
end
